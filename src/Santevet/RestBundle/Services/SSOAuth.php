<?php
/*
 *
  __/\\\\\\\\\\\\\\\__/\\\______________/\\\\____________/\\\\_______/\\\\\_______/\\\\___/\\\\\\\\\\\\\\\_
  ___\/\\\///////////__\/\\\_____________\/\\\\\\________/\\\\\\_____/\\\///\\\____\///\\__\/\\\///////////__
  ____\/\\\_____________\/\\\_____________\/\\\//\\\____/\\\//\\\___/\\\/__\///\\\___/\\/___\/\\\_____________
  _____\/\\\\\\\\\\\_____\/\\\_____________\/\\\\///\\\/\\\/_\/\\\__/\\\______\//\\\_\//_____\/\\\\\\\\\\\\_____
  ______\/\\\///////______\/\\\_____________\/\\\__\///\\\/___\/\\\_\/\\\_______\/\\\_________\////////////\\\___
  _______\/\\\_____________\/\\\_____________\/\\\____\///_____\/\\\_\//\\\______/\\\_____________________\//\\\__
  ________\/\\\_____________\/\\\_____________\/\\\_____________\/\\\__\///\\\__/\\\____________/\\\________\/\\\__
  _________\/\\\\\\\\\\\\\\\_\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\____\///\\\\\/____________\//\\\\\\\\\\\\\/___
  __________\///////////////__\///////////////__\///______________\///_______\/////_______________\/////////////_____
 *
 */

namespace Santevet\RestBundle\Services;

use Doctrine\ORM\EntityManager;

/**
 * Description of SSOAuth
 *
 * @author mokhtar
 */
class SSOAuth
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     *
     * @param array $options
     * @return array
     */
    public function validateTokenSession($options = [])
    {
        //init vars
        $status_code = 200;
        $return      = [];

        if (isset($options['token_session'])) {
            //recherche de User connecté avec le "token_session" fourni
            $current_session = $this->em->getRepository('SantevetRestBundle:Session')->findOneByToken($options['token_session']);

            $now = new \DateTime('now');
            if ($current_session) {
                //testet la validation de la Session (token_session)
                if ($current_session->getExpiredAt() < $now) {
                    $current_session->setExpired(true);
                    $this->em->persist($current_session);
                    $this->em->flush();
                    $this->em->refresh($current_session);
                }
                if ($current_session->getExpired()) {
                    $return['valid'] = false;
                } else {
                    $return['valid'] = true;
                }
            } else {
                $return['errors'][] = "Aucune session trouvée avec le token_session fourni";
                $status_code        = 400;
            }
        } else {
            $status_code = 400;
            $return['errors'][] = "paramètre manquant: token_session";
        }
        //retour de la resultat
        return [$status_code, $return];
    }

    /**
     *
     * @param array $options
     * @return array
     */
    public function generateTempToken($options)
    {
        //init vars
        $status_code = 200;
        $return      = [];
        if (isset($options['login']) && isset($options['token_session'])) {
//recherche de User connecté avec le "token_session" fourni
            $current_session = $this->em->getRepository('SantevetRestBundle:Session')->findOneByTokenLogin($options['token_session'],
                $options['login']);

            $now = new \DateTime('now');
            if ($current_session) {
//testet la validation de la Session (token_session)
                if ($current_session->getExpiredAt() < $now) {
                    $current_session->setExpired(true);
                    $this->em->persist($current_session);
                    $this->em->flush();
                    $this->em->refresh($current_session);
                }

                if ($current_session->getExpired()) {
                    $return['errors'][] = "Votre session a expiré";
                    $status_code        = 400;
                } else {

//generation d'un nouveau "token_temp"
                    $token = new \Santevet\RestBundle\Entity\Token();

                    $token->setToken(hash('md5', time()));
                    $token->setSession($current_session);

                    $now->modify('+3 minutes');
                    $token->setExpireAt($now);
                    $this->em->persist($token);
                    $this->em->flush();
                    $return["token_temp"] = $token->getToken();
                    $return["expiration"] = $token->getExpireAt()->format('d/m/Y H:i:s');
                }
            } else {
                $return['errors'][] = "Aucune session trouvée avec le token_session/login fournis";
                $status_code        = 400;
            }
        } else {
            $status_code = 400;
            if (!isset($options['token_session'])) {
                $return['errors'][] = "paramètre manquant: token_session";
            }
            if (!isset($options['login'])) {
                $return['errors'][] = "paramètre manquant: login";
            }
        }
        //retour de la resultat
        return [$status_code, $return];
    }

    public function loginToken($options = [])
    {
        //init vars
        $status_code = 200;
        $return      = [];

        if (isset($options['token_temp']) && isset($options['login']) && isset($options['url_logout'])) {
            //recherche et validation de token_temp
            $token = $this->em->getRepository('SantevetRestBundle:Token')->findOneBy(['token' => $options['token_temp']]);

            $now = new \DateTime('now');

            if ($token) {
                if ($token->getExpireAt() < $now) {
                    $return['errors'][] = array("Token_temp expiré");
                    $status_code        = 400;
                } else {
                    //creation d'un nouveau LogoutUrl
                    $logout_url = new \Santevet\RestBundle\Entity\LogoutUrl();
                    $logout_url->setSession($token->getSession());
                    $logout_url->setUrl($options['url_logout']);
                    $this->em->persist($logout_url);
                    $this->em->flush();

                    //creation d'une nouvelle session
                    $now         = new \DateTime('now');
                    $now->modify('+2 hours');
                    $new_session = new \Santevet\RestBundle\Entity\Session();
                    $new_session->setExpiredAt($now);
                    $new_session->setUser($token->getSession()->getUser());
                    $new_session->setToken(hash('md5', time()));
                    $new_session->setExpired(false);
                    $this->em->persist($new_session);
                    $this->em->flush();

                    $return["token_session"] = $new_session->getToken();
                    $return["validity"]      = $new_session->getExpiredAt()->format('d/m/Y H:i:s');
                }
            } else {
                $return['errors'][] = array("No token found with given parameters!");
                $status_code        = 400;
            }
        } else {
            $status_code = 400;
            if (!isset($options['token_temp'])) {
                $return['errors'][] = array("paramètre manquant: token_temp");
            }
            if (!isset($options['login'])) {
                $return['errors'][] = array("paramètre manquant: login");
            }
            if (!isset($options['url_logout'])) {
                $return['errors'][] = array("paramètre manquant: url_logout");
            }
        }
        //retour de la resultat
        return [$status_code, $return];
    }
}