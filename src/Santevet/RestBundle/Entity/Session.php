<?php

namespace Santevet\RestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * Session
 *
 * @ORM\Table(name="rest_sessions")
 * @ORM\Entity(repositoryClass="Santevet\RestBundle\Repositories\SessionRepository")
 *
 * @ExclusionPolicy("all")
 */
class Session
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255)
     * @Expose
     */
    private $token;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiredAt", type="datetime")
     * @Expose
     */
    private $expiredAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="expired", type="boolean", nullable=true)
     * @Expose
     */
    private $expired;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     * @Expose
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     * @Expose
     */
    private $updatedAt;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="sessions", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \LogoutUrl
     *
     * @ORM\OneToMany(targetEntity="LogoutUrl", mappedBy="session", cascade={"remove", "persist"})
     *
     */
    private $logoutUrls;

    /**
     * @var \Token
     *
     * @ORM\OneToMany(targetEntity="Token", mappedBy="session", cascade={"remove", "persist"})
     *
     */
    private $tokens;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime('now'));
        $this->setUpdatedAt(new \DateTime('now'));
        $this->logoutUrls = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tokens     = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Session
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set expiredAt
     *
     * @param \DateTime $expiredAt
     *
     * @return Session
     */
    public function setExpiredAt($expiredAt)
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    /**
     * Get expiredAt
     *
     * @return \DateTime
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * Set expired
     *
     * @param boolean $expired
     *
     * @return Session
     */
    public function setExpired($expired)
    {
        $this->expired = $expired;

        return $this;
    }

    /**
     * Get expired
     *
     * @return boolean
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Session
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Session
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param \Santevet\RestBundle\Entity\User $user
     *
     * @return Session
     */
    public function setUser(\Santevet\RestBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Santevet\RestBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add logoutUrl
     *
     * @param \Santevet\RestBundle\Entity\LogoutUrl $logoutUrl
     *
     * @return Session
     */
    public function addLogoutUrl(\Santevet\RestBundle\Entity\LogoutUrl $logoutUrl)
    {
        $this->logoutUrls[] = $logoutUrl;

        return $this;
    }

    /**
     * Remove logoutUrl
     *
     * @param \Santevet\RestBundle\Entity\LogoutUrl $logoutUrl
     */
    public function removeLogoutUrl(\Santevet\RestBundle\Entity\LogoutUrl $logoutUrl)
    {
        $this->logoutUrls->removeElement($logoutUrl);
    }

    /**
     * Get logoutUrls
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogoutUrls()
    {
        return $this->logoutUrls;
    }

    /**
     * Add token
     *
     * @param \Santevet\RestBundle\Entity\Token $token
     *
     * @return Session
     */
    public function addToken(\Santevet\RestBundle\Entity\Token $token)
    {
        $this->tokens[] = $token;

        return $this;
    }

    /**
     * Remove token
     *
     * @param \Santevet\RestBundle\Entity\Token $token
     */
    public function removeToken(\Santevet\RestBundle\Entity\Token $token)
    {
        $this->tokens->removeElement($token);
    }

    /**
     * Get tokens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTokens()
    {
        return $this->tokens;
    }
}