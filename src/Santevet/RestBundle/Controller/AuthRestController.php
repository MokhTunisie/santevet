<?php
/*
 *
  __/\\\\\\\\\\\\\\\__/\\\______________/\\\\____________/\\\\_______/\\\\\_______/\\\\___/\\\\\\\\\\\\\\\_
  ___\/\\\///////////__\/\\\_____________\/\\\\\\________/\\\\\\_____/\\\///\\\____\///\\__\/\\\///////////__
  ____\/\\\_____________\/\\\_____________\/\\\//\\\____/\\\//\\\___/\\\/__\///\\\___/\\/___\/\\\_____________
  _____\/\\\\\\\\\\\_____\/\\\_____________\/\\\\///\\\/\\\/_\/\\\__/\\\______\//\\\_\//_____\/\\\\\\\\\\\\_____
  ______\/\\\///////______\/\\\_____________\/\\\__\///\\\/___\/\\\_\/\\\_______\/\\\_________\////////////\\\___
  _______\/\\\_____________\/\\\_____________\/\\\____\///_____\/\\\_\//\\\______/\\\_____________________\//\\\__
  ________\/\\\_____________\/\\\_____________\/\\\_____________\/\\\__\///\\\__/\\\____________/\\\________\/\\\__
  _________\/\\\\\\\\\\\\\\\_\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\____\///\\\\\/____________\//\\\\\\\\\\\\\/___
  __________\///////////////__\///////////////__\///______________\///_______\/////_______________\/////////////_____
 *
 */

namespace Santevet\RestBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of AuthRestController
 *
 * @author mokhtar
 */
class AuthRestController extends FOSRestController
{

    /**
     * @return json
     * @Route("/validate/token/session")
     * @View()
     * @ApiDoc(
     *   description="Tester la validité d'un token_session fourni",
     *   resource = true,
     *   parameters={
     *     {"name"="token_session","dataType"="string","description"="token de la session","required"=true},
     *   },
     *   response = {
     *     {"name"="valid" , "dataType=boolean", "description"="validité d'un token_session fourni"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when failed"
     *   }
     * )
     */
    public function getValidateTokenSessionAction(Request $request)
    {
        //recuperation des parametres
        $options['token_session']   = $request->get('token_session');
        //appel au service et execution de la methode qui verifier la validité de token_session.
        list($status_code, $return) = $this->get('santevet_rest.sso_auth')->validateTokenSession($options);

        //retour de la réponse
        return $this->handleView($this->view($return, $status_code));
    }

    /**
     * @return json
     * @Route("/generate/token/temp")
     * @View()
     * @ApiDoc(
     *   description="Génération d'un token à partir de token_session/login fournis",
     *   resource = true,
     *   parameters={
     *     {"name"="token_session","dataType"="string","description"="token de la session","required"=true},
     *     {"name"="login","dataType"="string","description"="nom d'utilisateur","required"=true},
     *   },
     *   response = {
     *     {"name"="token_temp" , "dataType=struing", "description"="token_temp de Token généré"},
     *     {"name"="expiration" , "dataType=DateTime", "description"="date d'exipration de Token généré"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when failed"
     *   }
     * )
     */
    public function putGenerateTempTokenAction(Request $request)
    {
//recuperation des parametres
        $options['token_session'] = $request->get('token_session');
        $options['login']         = $request->get('login');
        //appel au service et execution de la methode qui génére un "token_temp" à partir de login/token_session.
        list($status_code, $return) = $this->get('santevet_rest.sso_auth')->generateTempToken($options);

        //retour de la réponse
        return $this->handleView($this->view($return, $status_code));
    }

    /**
     * @return json
     * @Route("/login/token")
     * @View()
     * @ApiDoc(
     *   description="Connexion de l'utilisateur (login/token_temp) à l'espace (url_login)",
     *   resource = true,
     *   parameters={
     *     {"name"="token_temp","dataType"="string","description"="token temporaire","required"=true},
     *     {"name"="login","dataType"="string","description"="nom d'utilisateur","required"=true},
     *     {"name"="url_logout","dataType"="string","description"="Url de déconnexion à utiliser lors de déconnexion globale","required"=true},
     *     {"name"="dataToLoad","dataType"="string","description"="","required"=false},
     *   },
     *   response = {
     *     {"name"="token_session" , "dataType=struing", "description"="token de la session"},
     *     {"name"="validity" , "dataType=DateTime", "description"="date d'exipration de la session"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when failed"
     *   }
     * )
     */
    public function postLoginTokenAction(Request $request)
    {
        //recuperation des parametres
        $options['token_temp']   = $request->get('token_temp');
        $options['login']        = $request->get('login');
        $options['url_logout']   = $request->get('url_logout');
        $options['data_to_load'] = $request->get('dataToLoad');
        //appel au service et execution de la methode qui génére un "token_temp" à partir de login/token_session.
        list($status_code, $return) = $this->get('santevet_rest.sso_auth')->loginToken($options);

//retour de la réponse
        return $this->handleView($this->view($return, $status_code));
    }
}