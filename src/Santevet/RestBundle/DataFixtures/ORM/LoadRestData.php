<?php
/*
 *
  __/\\\\\\\\\\\\\\\__/\\\______________/\\\\____________/\\\\_______/\\\\\_______/\\\\___/\\\\\\\\\\\\\\\_
  ___\/\\\///////////__\/\\\_____________\/\\\\\\________/\\\\\\_____/\\\///\\\____\///\\__\/\\\///////////__
  ____\/\\\_____________\/\\\_____________\/\\\//\\\____/\\\//\\\___/\\\/__\///\\\___/\\/___\/\\\_____________
  _____\/\\\\\\\\\\\_____\/\\\_____________\/\\\\///\\\/\\\/_\/\\\__/\\\______\//\\\_\//_____\/\\\\\\\\\\\\_____
  ______\/\\\///////______\/\\\_____________\/\\\__\///\\\/___\/\\\_\/\\\_______\/\\\_________\////////////\\\___
  _______\/\\\_____________\/\\\_____________\/\\\____\///_____\/\\\_\//\\\______/\\\_____________________\//\\\__
  ________\/\\\_____________\/\\\_____________\/\\\_____________\/\\\__\///\\\__/\\\____________/\\\________\/\\\__
  _________\/\\\\\\\\\\\\\\\_\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\____\///\\\\\/____________\//\\\\\\\\\\\\\/___
  __________\///////////////__\///////////////__\///______________\///_______\/////_______________\/////////////_____
 *
 */

namespace Santevet\RestBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Santevet\RestBundle\Entity\User;
use Santevet\RestBundle\Entity\Session;
use Santevet\RestBundle\Entity\Token;

/**
 * Description of LoadUserData
 *
 * @author mokhtar
 */
class LoadRestData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     *
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('test@santevet.com');
        $user->setPassword('test');
        $user->setLogin('santevet');
        $manager->persist($user);

        $next_year = new \DateTime('now');
        $next_year->modify('+1 year');

        $last_year = new \DateTime('now');
        $last_year->modify('-1 year');

        $session = new Session();
        $session->setUser($user);
        $session->setExpiredAt($next_year);
        $session->setExpired(false);
        $session->setToken('8b0d9c93a3545d0d99f6486208dd3659ec802ee1');
        $session->setCreatedAt(new \DateTime('now'));
        $session->setUpdatedAt(new \DateTime('now'));
        $manager->persist($session);

        $expired_session = new Session();
        $expired_session->setUser($user);
        $expired_session->setExpiredAt($last_year);
        $expired_session->setExpired(true);
        $expired_session->setToken('8b0d9c93a3545d0d99f6486208dd3659e454545');
        $expired_session->setCreatedAt($last_year);
        $expired_session->setUpdatedAt($last_year);
        $manager->persist($expired_session);

        $expired_token_temp = new Token();
        $expired_token_temp->setSession($session);
        $expired_token_temp->setExpireAt($last_year);
        $expired_token_temp->setCreatedAt($last_year);
        $expired_token_temp->setUpdatedAt($last_year);
        $expired_token_temp->setToken('8b0d9c93a3545d0d99f6486200000000000');
        $manager->persist($expired_token_temp);

        $manager->flush();
    }
}