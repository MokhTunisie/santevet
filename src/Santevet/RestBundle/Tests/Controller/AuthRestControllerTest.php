<?php
/*
 *
  __/\\\\\\\\\\\\\\\__/\\\______________/\\\\____________/\\\\_______/\\\\\_______/\\\\___/\\\\\\\\\\\\\\\_
  ___\/\\\///////////__\/\\\_____________\/\\\\\\________/\\\\\\_____/\\\///\\\____\///\\__\/\\\///////////__
  ____\/\\\_____________\/\\\_____________\/\\\//\\\____/\\\//\\\___/\\\/__\///\\\___/\\/___\/\\\_____________
  _____\/\\\\\\\\\\\_____\/\\\_____________\/\\\\///\\\/\\\/_\/\\\__/\\\______\//\\\_\//_____\/\\\\\\\\\\\\_____
  ______\/\\\///////______\/\\\_____________\/\\\__\///\\\/___\/\\\_\/\\\_______\/\\\_________\////////////\\\___
  _______\/\\\_____________\/\\\_____________\/\\\____\///_____\/\\\_\//\\\______/\\\_____________________\//\\\__
  ________\/\\\_____________\/\\\_____________\/\\\_____________\/\\\__\///\\\__/\\\____________/\\\________\/\\\__
  _________\/\\\\\\\\\\\\\\\_\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\____\///\\\\\/____________\//\\\\\\\\\\\\\/___
  __________\///////////////__\///////////////__\///______________\///_______\/////_______________\/////////////_____
 *
 */

namespace Santevet\RestBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Description of AuthRestControllerTest
 *
 * @author mokhtar
 */
class AuthRestControllerTest extends WebTestCase
{

    public function setUp()
    {
        $this->client = static::createClient();
    }

    protected function assertJsonResponse($response, $statusCode = 200)
    {
        $this->assertEquals(
            $statusCode, $response->getStatusCode(), $response->getContent()
        );
        $this->assertTrue(
            $response->headers->contains('Content-Type', 'application/json'),
            $response->headers
        );
    }

    public function testValidateTokenAction()
    {
        //init data pour les tests
        $fixtures = ['Santevet\RestBundle\DataFixtures\ORM\LoadRestData'];
        $this->loadFixtures($fixtures);

        /*         * ************** test si tout est bon ************** */
        $data     = [
            'token_session' => '8b0d9c93a3545d0d99f6486208dd3659ec802ee1'
        ];
        $route    = $this->getUrl('api_get_validate_token_session', $data, true);
        $this->client->request('GET', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();
        $this->assertJsonResponse($response, 200);
        $result   = json_decode($content, true);
        $this->assertArrayHasKey('valid', $result);
        $this->assertEquals(true, $result['valid']);

        /*         * ************** test si il'y à des données manquantes ************** */
        $route    = $this->getUrl('api_get_validate_token_session', []);
        $this->client->request('GET', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();
        $this->assertJsonResponse($response, 400);

        /*         * ************** test si le token_session est expiré ************** */
        $data     = [
            'token_session' => '8b0d9c93a3545d0d99f6486208dd3659e454545'
        ];
        $route    = $this->getUrl('api_get_validate_token_session', $data, true);
        $this->client->request('GET', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();
        $this->assertJsonResponse($response, 200);
        $result   = json_decode($content, true);
        $this->assertArrayHasKey('valid', $result);
        $this->assertEquals(false, $result['valid']);

        /*         * ************** test si le token_session est non valide ************** */
        $data     = [
            'token_session' => 'wrong_token'
        ];
        $route    = $this->getUrl('api_get_validate_token_session', $data, true);
        $this->client->request('GET', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();
        $this->assertJsonResponse($response, 400);
        $result   = json_decode($content, true);
        $this->assertArrayHasKey('errors', $result);
        $this->assertEquals(count($result['errors']), 1);
    }

    public function testGenerateTempTokenAction()
    {

        //init data pour les tests
        $fixtures = ['Santevet\RestBundle\DataFixtures\ORM\LoadRestData'];
        $this->loadFixtures($fixtures);

        /*         * ************** test si tout est bon ************** */
        $data = [
            'token_session' => '8b0d9c93a3545d0d99f6486208dd3659ec802ee1',
            'login' => 'santevet'
        ];

        $route = $this->getUrl('api_put_generate_temp_token', $data, true);

        $this->client->request('PUT', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();

        $this->assertJsonResponse($response, 200);
        $result = json_decode($content, true);
        $this->assertArrayHasKey('token_temp', $result);
        $this->assertArrayHasKey('expiration', $result);



        /*         * ************** test si il'y à des données manquantes ************** */
        $data = [];

        $route = $this->getUrl('api_put_generate_temp_token', $data, true);

        $this->client->request('PUT', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();

        $this->assertJsonResponse($response, 400);
        $result = json_decode($content, true);
        $this->assertEquals(count($result['errors']), 2);



        /*         * ************** test si le token_session est expiré ************** */
        $data = [
            "token_session" => "8b0d9c93a3545d0d99f6486208dd3659e454545",
            "login" => "santevet"
        ];

        $route = $this->getUrl('api_put_generate_temp_token', $data, true);

        $this->client->request('PUT', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();

        $this->assertJsonResponse($response, 400);
        $result = json_decode($content, true);
        $this->assertEquals(count($result['errors']), 1);



        /*         * ************** test si le token_session est non valide ************** */
        $data = [
            "token_session" => "wrong_token",
            "login" => "santevet"
        ];

        $route = $this->getUrl('api_put_generate_temp_token', $data, true);

        $this->client->request('PUT', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();

        $this->assertJsonResponse($response, 400);
        $result = json_decode($content, true);
        $this->assertEquals(count($result['errors']), 1);
    }

    public function testLoginTokenAction()
    {
        //init data pour les tests
        $fixtures = ['Santevet\RestBundle\DataFixtures\ORM\LoadRestData'];
        $this->loadFixtures($fixtures);

        /*         * ************** test si tout est bon ************** */
        //geneation de token_temp pour le test
        list($status_code, $return) = $this->getContainer()->get('santevet_rest.sso_auth')->generateTempToken([
            "token_session" => "8b0d9c93a3545d0d99f6486208dd3659ec802ee1",
            "login" => "santevet"
        ]);

        //test
        $data = [
            'token_temp' => $return['token_temp'],
            'login' => 'santevet',
            'url_logout' => 'http://some_logout_url',
            'dataToLoad' => null
        ];

        $route = $this->getUrl('api_post_login_token', $data, true);

        $this->client->request('POST', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();

        $this->assertJsonResponse($response, 200);
        $result = json_decode($content, true);
        $this->assertArrayHasKey('token_session', $result);
        $this->assertArrayHasKey('validity', $result);


        /*         * ************** test si il'y à des données manquantes ************** */
        $data = [
        ];

        $route = $this->getUrl('api_post_login_token', $data, true);

        $this->client->request('POST', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();

        $this->assertJsonResponse($response, 400);
        $result = json_decode($content, true);
        $this->assertArrayHasKey('errors', $result);
        $this->assertEquals(count($result['errors']), 3);


        /*         * ************** test si le token_temp est expiré ************** */
        $data = [
            "token_temp" => "8b0d9c93a3545d0d99f6486200000000000",
            "login" => "santevet",
            "url_logout" => "some_url"
        ];

        $route = $this->getUrl('api_post_login_token', $data, true);

        $this->client->request('POST', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();

        $this->assertJsonResponse($response, 400);
        $result = json_decode($content, true);
        $this->assertArrayHasKey('errors', $result);
        $this->assertEquals(count($result['errors']), 1);


        /*         * ************** test si le token_temp est non valide ************** */
        $data = [
            "token_temp" => "wrong_token",
            "login" => "santevet",
            "url_logout" => "some_url"
        ];

        $route = $this->getUrl('api_post_login_token', $data, true);

        $this->client->request('POST', $route, ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content  = $response->getContent();

        $this->assertJsonResponse($response, 400);
        $result = json_decode($content, true);
        $this->assertArrayHasKey('errors', $result);
        $this->assertEquals(count($result['errors']), 1);
    }
}