<?php
/*
 *
  __/\\\\\\\\\\\\\\\__/\\\______________/\\\\____________/\\\\_______/\\\\\_______/\\\\___/\\\\\\\\\\\\\\\_
  ___\/\\\///////////__\/\\\_____________\/\\\\\\________/\\\\\\_____/\\\///\\\____\///\\__\/\\\///////////__
  ____\/\\\_____________\/\\\_____________\/\\\//\\\____/\\\//\\\___/\\\/__\///\\\___/\\/___\/\\\_____________
  _____\/\\\\\\\\\\\_____\/\\\_____________\/\\\\///\\\/\\\/_\/\\\__/\\\______\//\\\_\//_____\/\\\\\\\\\\\\_____
  ______\/\\\///////______\/\\\_____________\/\\\__\///\\\/___\/\\\_\/\\\_______\/\\\_________\////////////\\\___
  _______\/\\\_____________\/\\\_____________\/\\\____\///_____\/\\\_\//\\\______/\\\_____________________\//\\\__
  ________\/\\\_____________\/\\\_____________\/\\\_____________\/\\\__\///\\\__/\\\____________/\\\________\/\\\__
  _________\/\\\\\\\\\\\\\\\_\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\____\///\\\\\/____________\//\\\\\\\\\\\\\/___
  __________\///////////////__\///////////////__\///______________\///_______\/////_______________\/////////////_____
 *
 */

namespace Santevet\RestBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Description of SSOAuthTest
 *
 * @author mokhtar
 */
class SSOAuthUnitTest extends KernelTestCase
{
    private $container;

    public function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }

    public function testValidateTokenSession()
    {
        /*         * ************** test si tout est bon ************** */
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->validateTokenSession([
            "token_session" => "8b0d9c93a3545d0d99f6486208dd3659ec802ee1",
        ]);
        $this->assertEquals($status_code, 200);
        $this->assertArrayHasKey('valid', $return);
        $this->assertEquals($return['valid'], true);
        
        /*         * ************** test si le token_session est expiré ************** */
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->validateTokenSession([
            "token_session" => "8b0d9c93a3545d0d99f6486208dd3659e454545",
        ]);
        $this->assertEquals($status_code, 200);
        $this->assertArrayHasKey('valid', $return);
        $this->assertEquals($return['valid'], false);
        
        /*         * ************** test si le token_session est non valide ************** */
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->validateTokenSession([
            "token_session" => "wrong_token",
        ]);
        $this->assertEquals($status_code, 400);
        $this->assertEquals(count($return['errors']), 1);

        /*         * ************** test si il'y à des données manquantes ************** */
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->validateTokenSession([
        ]);
        $this->assertEquals($status_code, 400);
        $this->assertEquals(count($return['errors']), 1);
    }

    public function testGenerateTempToken()
    {
        /*         * ************** test si tout est bon ************** */
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->generateTempToken([
            "token_session" => "8b0d9c93a3545d0d99f6486208dd3659ec802ee1",
            "login" => "santevet"
        ]);
        $this->assertEquals($status_code, 200);
        $this->assertArrayHasKey('token_temp', $return);
        $this->assertArrayHasKey('expiration', $return);
        /*         * ************** test si il'y à des données manquantes ************** */
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->generateTempToken([
        ]);
        $this->assertEquals($status_code, 400);
        $this->assertEquals(count($return['errors']), 2);
        /*         * ************** test si le token_session est expiré ************** */
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->generateTempToken([
            "token_session" => "8b0d9c93a3545d0d99f6486208dd3659e454545",
            "login" => "santevet"
        ]);
        $this->assertEquals($status_code, 400);
        $this->assertEquals(count($return['errors']), 1);
        /*         * ************** test si le token_session est non valide ************** */
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->generateTempToken([
            "token_session" => "wrong_token",
            "login" => "santevet"
        ]);
        $this->assertEquals($status_code, 400);
        $this->assertEquals(count($return['errors']), 1);
    }

    public function testLoginToken()
    {
        /*         * ************** test si tout est bon ************** */
        //geneation de token_temp pour le test
        list($status_code_token_temp, $return_token_temp) = $this->container->get('santevet_rest.sso_auth')->generateTempToken([
            "token_session" => "8b0d9c93a3545d0d99f6486208dd3659ec802ee1",
            "login" => "santevet"
        ]);
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->loginToken([
            "token_temp" => $return_token_temp['token_temp'],
            "login" => "santevet",
            "url_logout" => "some_url"
        ]);

        $this->assertEquals($status_code, 200);
        $this->assertArrayHasKey('token_session', $return);
        $this->assertArrayHasKey('validity', $return);

        /*         * ************** test si il'y à des données manquantes ************** */
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->loginToken([
        ]);
        $this->assertEquals($status_code, 400);
        $this->assertEquals(count($return['errors']), 3);

        /*         * ************** test si le token_temp est expiré ************** */
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->loginToken([
            "token_temp" => "8b0d9c93a3545d0d99f6486200000000000",
            "login" => "santevet",
            "url_logout" => "some_url"
        ]);
        $this->assertEquals($status_code, 400);
        $this->assertEquals(count($return['errors']), 1);

        /*         * ************** test si le token_temp est non valide ************** */
        list($status_code, $return) = $this->container->get('santevet_rest.sso_auth')->loginToken([
            "token_temp" => "wrong_token",
            "login" => "santevet",
            "url_logout" => "some_url"
        ]);
        $this->assertEquals($status_code, 400);
        $this->assertEquals(count($return['errors']), 1);
    }
}