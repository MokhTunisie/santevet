# Intallation de projet #

```
#!shell

#accedez au dossier de projet
git clone https://MokhTunisie@bitbucket.org/MokhTunisie/santevet.git
composer install
```

# Configuration de base de donnée #
- Lors d'execution de composer install, vous devez saisir le nom de base de données avec les paramètres liés.


```
#!shell
php app/console doctrine:database:create
php app/console doctrine:schema:update --force 
```


Description de l'api REST: Accedez à cet url: url_de_votre_projet/web/app_dev.php/api/doc

# Tests #
#Configuration de base de donnée pour les tests

```
#!shell

#accedez au dossier de projet
php app/console doctrine:database:create -e test
php app/console doctrine:schema:update --force -e test
```

#Execution des test avec phpunit

```
#!shell

#accedez au dossier de projet
phpunit src/Santevet/RestBundle/
```